// Поясніть своїми словами, як ви розумієте поняття асинхронності у Javascript
// Асинхронність дозволяє скрипту не чекати доки виконається функція, а йти далі по коду
div = document.getElementById("rezult")
button=document.getElementById("button")
button.onclick=getIp

async function getIp(){
    try {
        let api = await fetch('https://api.ipify.org/?format=json');
        let ip = (await api.json()).ip;
        let response = await fetch(`http://ip-api.com/json/${ip}`);
        let adres = await response.json();
        div.textContent=`${adres.country}
                                ${adres.regionName}
                                ${adres.city}`
    }
    catch (error){
        console.log(error)
    }
}

